// Freeze object with Object.freeze
const account = {
    username: "marijn",
    password: "xyzzy"
  }
Object.freeze(account)
account.password = "s3cret" // (*much* more secure)

console.log(account.password)
