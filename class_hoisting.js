let s = new Something()

class Something {}

// they are hoisted (the variable binding is available in the whole scope)
// just like let and const are - they only are not initialised.

// Internally your class is converted to function with the same name inside wrapper function(iife)
// and that wrapper function returns your function.
// Because your function's(class) scope is changed and you are trying to create object of function in global scope,
//  which is in reality not exist.

